import React from "react";
import { Link } from "react-router-dom";
import paysage from "./images/paysage.jpg";
import eu from "./images/eu.jpg";
import maroc from "./images/maroc.jpg";
import taj from "./images/taj.jpg";
import "./css/LandingPage.css";
import Search from './component/Search';

export default function LandingPage() {
  return (
    <div>
      <Search />
      <div className="Bloc1">
        <h1 className="texte1">Partez pour des destinations de reve</h1>
        <button className="Bouton">Rechercher une destination</button>
        <img src={paysage} className="paysage" alt="toto" />
      </div>
      <h2 className="Texte2">Nos destinations</h2>
      <div className="Bloc2">
        <img src={taj} className="taj" alt="toto" />
        <img src={eu} className="eu" alt="toto" />
        <img src={maroc} className="maroc" alt="toto" />
      </div>
      <div className="Bloc3">
        <h1 className="texte3">A propos de nous</h1>
        <Link to="/faq" className="LienFAQ">
          <h1>FAQ</h1>
        </Link>
        <Link to="/PageErreur" className="LienErreur">
          <h1>Erreur</h1>
        </Link>
      </div>
    </div>
  );
}
