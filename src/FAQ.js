import React from "react";
import "./css/FAQ.css";

export default function FAQ() {
  return (
    <div className="Bloc1FAQ">
      <h1>Qui nous sommes</h1>
      <p>
        Nous savons que vous ne manquez ni d’offres intéressantes, ni de
        services sur-mesure. Nous savons aussi que vous êtes habitués à pouvoir
        tout choisir, à tout moment, depuis chez vous. Et que l’agence de
        voyages, c’est un peu contraignant ou ce n’est juste pas vraiment vous.
        Alors, parce que nous ne voyageons plus aujourd’hui comme avant, nous
        nous réinventons et créons le Réseau des Nouveaux Voyageurs. Un réseau
        qui ne va pas essayer de vous vendre à tout prix une destination ou une
        solution miracle. Mais qui va relever un défi presque révolutionnaire
        dans le monde du voyage : vous être utile. Tout simplement. Pour qu’à
        tout moment vous puissiez avoir le conseil, l’offre ou les informations
        qui vous permettront de créer, recréer, réserver et mieux vivre tous vos
        voyages.
      </p>
      <h1>Nous sommes différents de la concurrence</h1>
      <p></p>
    </div>
  );
}
