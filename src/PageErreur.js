import React from "react";
import { Link } from "react-router-dom";
import "./css/PageErreur.css";

function PageErreur() {
  return (
    <div>
      <div className="BlocErreur">
        <h1>Oups</h1>
        <p>La page que vous recherchiez semble introuvable</p>
        <h2>Code d'erreur 404</h2>
      </div>
      <div>
        <h2>Voici quelques liens utiles à la place :</h2>
        <Link to="/FAQ" className="LienFAQ">
          <h1>FAQ</h1>
        </Link>
      </div>
    </div>
  );
}

export default PageErreur;
