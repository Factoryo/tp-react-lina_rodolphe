import React, { useState } from "react";
import "../css/Search.css";
import data from "../data.json";

function Search() {
  const [searchTerm, setSearchTerm] = useState("");
  return (
    <>
      <div className="container">
        <div className="searchInput_Container">
          <input
            id="searchInput"
            type="text"
            placeholder="Search here..."
            onChange={(event) => {
              setSearchTerm(event.target.value);
            }}
          />
        </div>

        <div className="data_Container">
          {data
          // eslint-disable-next-ligne
            .filter((val) => {
              if (searchTerm === "") {
                return val;
              } else if (
                val.place.toLowerCase().includes(searchTerm.toLowerCase())
              ) {
                return val;
              }
              return null
            })
            .map((val) => {
              return (
                <div className="data" key={val.id}>
                  <img src={val.image} alt="" />
                  <h3>{val.place}</h3>
                  <p className="price">{val.country}</p>
                </div>
              );
            })}
        </div>
      </div>
    </>
  );
}

export default Search;
