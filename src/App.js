import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import LandingPage from "./LandingPage";
import FAQ from "./FAQ";
import PageErreur from "./PageErreur";

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/" component={LandingPage} />
          <Route path="/faq" component={FAQ} />
          <Route path="*" component={PageErreur} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
